import {Injectable} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {ErrorDialogComponent} from '../../components/error-dialog/error-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class GlobalHttpInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    private dialog: MatDialog
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      catchError((error) => {
        if (error instanceof HttpErrorResponse) {
          this.dialog.open(ErrorDialogComponent, {
            data: {
              error: error.error,
              status: error.status,
            }
          });
          if (error.error instanceof ErrorEvent) {
            // console.error('Error Event');
          } else {
            // console.log(`error status : ${error.status} ${error.statusText}`);
            switch (error.status) {
              case 401:      //login
                // this.router.navigateByUrl('/login');
                break;
              case 403:     //forbidden
                // this.router.navigateByUrl('/login');
                break;
            }
          }
        } else {
          // console.error('some thing else happened');
        }
        return throwError(error);
      })
    );
  }
}

export const globalHttpInterceptorProviders = [
  {provide: HTTP_INTERCEPTORS, useClass: GlobalHttpInterceptor, multi: true}
];

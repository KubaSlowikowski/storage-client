import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProductGroup} from '../../interfaces/product-group';
import {Page} from "../../interfaces/page";
import {Message} from '../../interfaces/message';

const API_URL = 'http://localhost:8080/api/groups';

@Injectable({
  providedIn: 'root'
})
export class ProductGroupService {

  constructor(private httpClient: HttpClient) {
  }

  public getAllProductGroups(page: Page<ProductGroup>, search: string = ''): Observable<Page<ProductGroup>> {
    return this.httpClient.get<Page<ProductGroup>>(API_URL + page.buildPageableUrl() + search, {withCredentials: true});
  }

  public getProductGroupById(id: number): Observable<ProductGroup> {
    return this.httpClient.get<ProductGroup>(API_URL + '/' + id, {withCredentials: true});
  }

  public addProductGroup(productGroup: ProductGroup): Observable<any> {
    return this.httpClient.post(API_URL, productGroup, {withCredentials: true});
  }

  public updateProductGroup(productGroup: ProductGroup): Observable<any> {
    return this.httpClient.put(API_URL + '/' + productGroup.id, productGroup, {withCredentials: true});
  }

  public deleteProductGroup(id: number): Observable<any> {
    return this.httpClient.delete(API_URL + '/' + id);
  }

  public getAsPdfReport(pageableUrl: any, search: string = ''): Observable<Blob> {
    return this.httpClient.get(API_URL + '/pdf' + pageableUrl + search, {withCredentials: true, responseType: 'blob'});
  }

  public getAsXlsxReport(pageableUrl: any, search: string = ''): Observable<Blob> {
    return this.httpClient.get(API_URL + '/excel' + pageableUrl + search, {withCredentials: true, responseType: 'blob'});
  }

  public sendAllInMail(pageableUrl: any, search: string = '', message: Message): Observable<any> {
    return this.httpClient.get(API_URL + '/email' + pageableUrl + search + message.toMessageUrl(), {withCredentials: true});
  }
}

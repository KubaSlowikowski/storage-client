import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor() { }

  public addErrorDescription(status: number): string {
    switch(status) {
      case 0:
        return 'Connection refused.';
      case 400:
        return 'Bad request.';
      case 403:
        return 'You have no permission to access this content.';
      default:
        return null;
    }
  }
}

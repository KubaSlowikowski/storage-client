import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Product} from '../../interfaces/product';
import {Page} from '../../interfaces/page';
import {TokenStorageService} from '../token-storage/token-storage.service';
import {Message} from '../../interfaces/message';

const API_URL = 'http://localhost:8080/api/products';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient: HttpClient) {
  }

  public getAllProducts(page: Page<Product>, search: string = ''): Observable<Page<Product>> {
    return this.httpClient.get<Page<Product>>(API_URL + page.buildPageableUrl() + search, {withCredentials: true});
  }

  public getProductById(id: number): Observable<Product> {
    return this.httpClient.get<Product>(API_URL + '/' + id, {withCredentials: true});
  }

  public getProductsByGroupId(groupId: number, page: Page<Product>): Observable<Page<Product>> {
    return this.httpClient.get<Page<Product>>(API_URL + '/all/' + groupId + page.buildPageableUrl(), {withCredentials: true});
  }

  public addProduct(product: Product): Observable<any> {
    return this.httpClient.post(API_URL, product, {withCredentials: true});
  }

  public updateProduct(product: Product): Observable<any> {
    return this.httpClient.put(API_URL + '/' + product.id, product, {withCredentials: true});
  }

  public deleteProduct(id: number): Observable<any> {
    return this.httpClient.delete(API_URL + '/' + id);
  }

  public buyProduct(id: number): Observable<any> {
    return this.httpClient.post(API_URL + '/' + id, {withCredentials: true});
  }

  public getAsPdfReport(pageableUrl: any, search: string = ''): Observable<Blob> {
    return this.httpClient.get(API_URL + '/pdf' + pageableUrl + search, {withCredentials: true, responseType: 'blob'});
  }

  public getAsXlsxReport(pageableUrl: any, search: string = ''): Observable<Blob> {
    return this.httpClient.get(API_URL + '/excel' + pageableUrl + search, {withCredentials: true, responseType: 'blob'});
  }

  public sendAllInMail(pageableUrl: any, search: string = '', message: Message): Observable<any> {
    return this.httpClient.get(API_URL + '/email' + pageableUrl + search + message.toMessageUrl(), {withCredentials: true});
  }
}

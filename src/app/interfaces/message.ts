export class Message {
  subject: string;
  text: string;
  readonly sendFrom: string;
  sendTo: string;
  fileExtension: string;

  constructor() {
    this.sendFrom = 'ttpraktyki2020storage@gmail.com';
    this.subject = 'Message from Angular';
    this.text = 'Hello! This mail comes from Angular';
    this.fileExtension = 'PDF';
  }

  public toMessageUrl(): string {
    if (this.sendTo === null && this.fileExtension === null) {
      console.log('null in message!!!');
      return;
    }
    let url = '&';
    url += 'subject=' + this.subject;
    url += '&text=' + this.text;
    url += '&sendFrom=' + this.sendFrom;
    url += '&sendTo=' + this.sendTo;
    url += '&file.fileExtension=' + this.fileExtension;
    return url;
  }
}

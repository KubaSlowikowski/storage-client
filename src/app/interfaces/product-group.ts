import {Product} from "./product";

export class ProductGroup {
  id: number;
  name: string;
  description: string;
  products: Product[];
}

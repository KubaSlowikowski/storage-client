import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductGroupTableOptionsDialogComponent } from './product-group-table-options-dialog.component';

describe('ProductGroupTableOptionsDialogComponent', () => {
  let component: ProductGroupTableOptionsDialogComponent;
  let fixture: ComponentFixture<ProductGroupTableOptionsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductGroupTableOptionsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductGroupTableOptionsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

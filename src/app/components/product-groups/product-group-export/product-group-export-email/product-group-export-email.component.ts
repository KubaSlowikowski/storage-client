import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';
import {Message} from '../../../../interfaces/message';
import {ProductGroupService} from '../../../../services/product-group/product-group.service';

@Component({
  selector: 'app-product-group-export-email',
  templateUrl: './product-group-export-email.component.html',
  styleUrls: ['./product-group-export-email.component.css']
})
export class ProductGroupExportEmailComponent {
  email = new FormControl('', [Validators.required, Validators.email]);

  constructor(
    public emailDialog: MatDialogRef<ProductGroupExportEmailComponent>,
    @Inject(MAT_DIALOG_DATA) public url: any,
    private productGroupService: ProductGroupService
  ) { }

  onSubmit() {
    if (!this.email.valid) {
      return;
    }
    let message = new Message();
    message.sendTo = this.email.value;
    this.productGroupService.sendAllInMail(this.url.pageable, this.url.searchParam, message).subscribe();
    this.emailDialog.close();
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }

  closeDialog() {
    this.emailDialog.close();
  }
}

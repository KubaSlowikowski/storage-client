import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductGroupExportEmailComponent} from './product-group-export-email.component';

describe('ProductGroupExportEmailComponent', () => {
  let component: ProductGroupExportEmailComponent;
  let fixture: ComponentFixture<ProductGroupExportEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductGroupExportEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductGroupExportEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

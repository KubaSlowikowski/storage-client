import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ProductGroupService} from '../../../../services/product-group/product-group.service';
import {ProductGroupExportEmailComponent} from '../product-group-export-email/product-group-export-email.component';

@Component({
  selector: 'app-product-group-export',
  templateUrl: './product-group-export.component.html',
  styleUrls: ['./product-group-export.component.css']
})
export class ProductGroupExportComponent {

  constructor(
    public downloadDialog: MatDialogRef<ProductGroupExportComponent>,
    @Inject(MAT_DIALOG_DATA) public url: any,
    private productGroupService: ProductGroupService,
    private emailDialog: MatDialog
  ) {
  }

  closeDialog() {
    this.downloadDialog.close();
  }

  downloadAsPdf() {
    this.productGroupService.getAsPdfReport(this.url.pageable, this.url.searchParam).subscribe(x => {
      const blob = new Blob([x], {type: 'application/pdf'});

      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob);
        return;
      }
      const data = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = data;
      link.download = 'testName.pdf';
      link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

      setTimeout(function() {
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    });

    this.downloadDialog.close();
  }

  downloadAsExcel() {
    this.productGroupService.getAsXlsxReport(this.url.pageable, this.url.searchParam).subscribe(x => {
      const blob = new Blob([x], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});

      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob);
        return;
      }
      const data = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = data;
      link.download = 'testName.xlsx';
      link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

      setTimeout(function() {
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    });
  }

  sendInMail() {
    this.emailDialog.open(ProductGroupExportEmailComponent, {
      data: {
        pageable: this.url.pageable,
        searchParam: this.url.searchParam
      }
    });
    this.downloadDialog.close();
  }
}

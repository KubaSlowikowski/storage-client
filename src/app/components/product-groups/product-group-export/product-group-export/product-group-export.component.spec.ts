import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductGroupExportComponent} from './product-group-export.component';

describe('ProductGroupExportComponent', () => {
  let component: ProductGroupExportComponent;
  let fixture: ComponentFixture<ProductGroupExportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductGroupExportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductGroupExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

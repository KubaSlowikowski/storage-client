import {Component, OnInit, ViewChild} from '@angular/core';
import {ProductGroupService} from '../../../services/product-group/product-group.service';
import {ProductGroup} from '../../../interfaces/product-group';
import {Product} from '../../../interfaces/product';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {Sort, SortDirection} from '@angular/material/sort';
import {ProductService} from '../../../services/product/product.service';
import {Page} from '../../../interfaces/page';
import {MatDialog} from '@angular/material/dialog';
import {ProductDetailsComponent} from '../../products/product-details/product-details.component';
import {ProductGroupExportComponent} from '../product-group-export/product-group-export/product-group-export.component';
import {ProductTableOptionsDialogComponent} from '../../products/product-table-options-dialog/product-table-options-dialog.component';
import {ProductGroupTableOptionsDialogComponent} from '../product-group-table-options-dialog/product-group-table-options-dialog.component';

@Component({
  selector: 'app-product-group',
  templateUrl: './product-group.component.html',
  styleUrls: ['./product-group.component.css']
})
export class ProductGroupComponent implements OnInit {
  columnsToDisplay: string[] = ['id', 'name', 'description', 'products', 'options'];
  dataSource: ProductGroup[] = [];
  products: Product[] = [];
  productsMap: Map<number, boolean> = new Map<number, boolean>();
  page: Page<ProductGroup> = new Page<ProductGroup>();
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;
  sort: Sort = new class implements Sort {
    active: string = 'id';
    direction: SortDirection = 'asc';
  };
  searchParam: string;
  descriptionMap: Map<number, boolean> = new Map<number, boolean>();
  optionsMap: Map<string, boolean> = new Map<string, boolean>();

  constructor(
    private productGroupService: ProductGroupService,
    private productService: ProductService,
    private downloadDialog: MatDialog,
    private optionsDialog: MatDialog
  ) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit(): void {
    this.page.size = 10;
    this.page.number = 0;
    this.refreshData();
  }

  deleteProductGroup(productGroup: ProductGroup) {
    this.productGroupService.deleteProductGroup(productGroup.id).subscribe(
      () => {
        this.refreshData();
      }
    );
  }

  findProductsByGroupId(id: number): void {
    this.productService.getProductsByGroupId(id, new Page()).subscribe(
      (receivedProducts) => {
        this.products = receivedProducts.content;
        this.refreshData();
      }
    );
    this.productsMap.clear();
    this.productsMap.set(id, true);
  }

  hideProducts(id: number) {
    this.productsMap.set(id, false);
  }

  refreshData(event?: PageEvent) {
    if (event) {
      this.page.number = event.pageIndex;
      this.page.size = event.pageSize;
    }

    if (this.sort.direction === '') {
      this.page.isSorted = false;
    } else {
      this.page.isSorted = true;
    }
    this.page.sortedBy = this.sort.active;
    this.page.dir = this.sort.direction;

    this.productGroupService.getAllProductGroups(this.page, this.searchParam).subscribe(response => {
      this.page.fromResponse(response);
      this.dataSource = response.content;
      this.pageIndex = response.number;
      this.pageSize = response.size;
      this.length = response.totalElements;

      this.dataSource.forEach(value => {
        this.descriptionMap.set(value.id, false)
      });
    });
    return event;
  }

  openProductDetails(product: Product) {
    this.downloadDialog.open(ProductDetailsComponent, {
      data: {
        id: product.id,
        name: product.name,
        description: product.description,
        price: product.price,
        groupId: product.groupId,
        sold: product.sold
      }
    });
  }

  sortData(sort: Sort) {
    this.sort = sort;
    this.refreshData();
  }

  openDownloadDialog() {
    this.refreshData();
    this.downloadDialog.open(ProductGroupExportComponent, {
      data: {
        pageable: this.page.buildPageableUrl(),
        searchParam: this.searchParam
      }
    });
  }

  applyFilter = (event: KeyboardEvent) => {
    if (event.key === 'Enter') {
      const filterValue = (event.target as HTMLInputElement).value;
      console.log(this.page)
      this.refreshData();
      console.log(this.page)
      this.searchParam = '&search='.concat(filterValue);
      this.productGroupService.getAllProductGroups(this.page, this.searchParam).subscribe(response => {
          this.dataSource = response.content;
        }
      );
    }
  }

  showDescription(id: any) {
    this.descriptionMap.set(id, true);
  }

  hideDescription(id: any) {
    this.descriptionMap.set(id, false);
  }

  openTableOptionsDialog() {
    this.optionsDialog.open(ProductGroupTableOptionsDialogComponent, {
      data: {
        descriptionMap: this.descriptionMap,
        optionsMap: this.optionsMap
      }
    })
  }
}

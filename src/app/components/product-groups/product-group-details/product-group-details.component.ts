import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {ProductGroup} from '../../../interfaces/product-group';

@Component({
  selector: 'app-product-group-details',
  templateUrl: './product-group-details.component.html',
  styleUrls: ['./product-group-details.component.css']
})
export class ProductGroupDetailsComponent {

  constructor(
    private dialogRef: MatDialogRef<ProductGroupDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public group: ProductGroup,
    private router: Router
  ) { }

  goToUpdateComponent() {
    this.router.navigate(['/groups/update/', this.group.id]);
    this.closeDialog();
  }

  closeDialog() {
    this.dialogRef.close();
  }
}

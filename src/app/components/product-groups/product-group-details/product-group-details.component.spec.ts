import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductGroupDetailsComponent} from './product-group-details.component';

describe('ProductGroupDetailsComponent', () => {
  let component: ProductGroupDetailsComponent;
  let fixture: ComponentFixture<ProductGroupDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductGroupDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductGroupDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

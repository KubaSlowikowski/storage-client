import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProductGroupService} from '../../../services/product-group/product-group.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductGroup} from '../../../interfaces/product-group';
import {Product} from '../../../interfaces/product';
import {ProductService} from '../../../services/product/product.service';
import {Page} from '../../../interfaces/page';
import {Location} from '@angular/common';

@Component({
  selector: 'app-product-group-create',
  templateUrl: './product-group-create.component.html',
  styleUrls: ['./product-group-create.component.css']
})
export class ProductGroupCreateComponent implements OnInit {
  NO_PRODUCT: string = 'No product';
  editProductGroupForm: FormGroup;
  id: number;
  isFormSubmitted = false;
  availableProducts: Product[];

  constructor(
    private formBuilder: FormBuilder,
    private productGroupService: ProductGroupService,
    private productService: ProductService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location,
  ) {
  }

  ngOnInit(): void {
    this.editProductGroupForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      description: ['', [Validators.required, Validators.maxLength(100)]],
      products: ['', []],
    });

    this.productService.getAllProducts(new Page<Product>()).subscribe(response => {
      this.availableProducts = response.content;
    })
  }

  submitForm() {
    this.isFormSubmitted = true;
    if (this.editProductGroupForm.invalid) {
      return;
    }

    const changedProductGroup = this.convertFormToProductGroup(this.editProductGroupForm);

    this.productGroupService.addProductGroup(changedProductGroup).subscribe(
      () => {
        this.router.navigate(['/groups']);
      }
    )
  }

  convertFormToProductGroup(productGroupForm: FormGroup): ProductGroup {
    if (productGroupForm.value.products == '') {
      productGroupForm.value.products = null;
    }
    return {
      id: null,
      name: productGroupForm.value.name,
      description: productGroupForm.value.description,
      products: productGroupForm.value.products
    };
  }

  goBack() {
    this.location.back();
  }
}

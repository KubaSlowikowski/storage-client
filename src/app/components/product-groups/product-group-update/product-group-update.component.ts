import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProductGroupService} from '../../../services/product-group/product-group.service';
import {ProductGroup} from '../../../interfaces/product-group';
import {Product} from '../../../interfaces/product';
import {ProductService} from '../../../services/product/product.service';
import {Page} from '../../../interfaces/page';
import {Location} from '@angular/common';

@Component({
  selector: 'app-product-group-update',
  templateUrl: './product-group-update.component.html',
  styleUrls: ['./product-group-update.component.css']
})
export class ProductGroupUpdateComponent implements OnInit {
  editProductGroupForm: FormGroup;
  id: number;
  isFormSubmitted = false;
  availableProducts: Product[];
  selectedProducts: Product[];

  constructor(
    private formBuilder: FormBuilder,
    private productGroupService: ProductGroupService,
    private productService: ProductService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location,

  ) {
  }

  ngOnInit(): void {
    this.editProductGroupForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      description: ['', [Validators.required, Validators.maxLength(100)]],
      products: ['', []],
    });
    this.activatedRoute.paramMap.subscribe(params => {
      this.id = +params.get('id');
      this.productGroupService.getProductGroupById(this.id).subscribe(
        receivedProductGroup => {
          this.selectedProducts = receivedProductGroup.products;
          this.editProductGroupForm.setValue({
            name: receivedProductGroup.name,
            description: receivedProductGroup.description,
            products: receivedProductGroup.products,
          });
        }
      );
    });

    this.productService.getAllProducts(new Page<Product>(1000)).subscribe(response => { //FIXME replace initial page size to  resizeble mat-select
      this.availableProducts = response.content;
    });
  }

  submitForm() {
    this.isFormSubmitted = true;
    if (this.editProductGroupForm.invalid) {
      return;
    }

    const changedProductGroup = this.convertFormToProductGroup(this.editProductGroupForm);

    this.productGroupService.updateProductGroup(changedProductGroup).subscribe(
      () => {
        this.router.navigate(['/groups']);
      }
    );
  }

  convertFormToProductGroup(productGroupForm: FormGroup): ProductGroup {
    return {
      id: this.id,
      name: productGroupForm.value.name,
      description: productGroupForm.value.description,
      products: productGroupForm.value.products
    };
  }

  goBack() {
    this.location.back();
  }

  compareProducts(p1: Product, p2: Product): boolean { //used to preselect values in <mat-select> element
    return p1 && p2 ? p1.id === p2.id : p1 === p2;
  }
}

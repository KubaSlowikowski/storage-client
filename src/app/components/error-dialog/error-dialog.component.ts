import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ErrorService} from '../../services/error/error.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.css']
})
export class ErrorDialogComponent implements OnInit {
  errorDescription: string;

  constructor(
    private dialogRef: MatDialogRef<ErrorDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public error: any,
    @Inject(MAT_DIALOG_DATA) public status: number,
    private errorService: ErrorService,
    private router: Router
  ) {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    if (JSON.stringify(this.error.error.error) === undefined) {
      this.error.error = JSON.stringify(this.error.error);
    } else {
      this.error.error = JSON.stringify(this.error.error.error);
    }
    this.errorDescription = this.errorService.addErrorDescription(this.error.status);
  }

  goToLogin() {
    this.closeDialog();
    this.router.navigateByUrl('/login');
  }
}

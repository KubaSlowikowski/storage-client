import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  currentUser: any;

  constructor(private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.currentUser = JSON.parse(sessionStorage.getItem('auth-user'));
  }

  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
}

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-product-table-options-dialog',
  templateUrl: './product-table-options-dialog.component.html',
  styleUrls: ['./product-table-options-dialog.component.css']
})
export class ProductTableOptionsDialogComponent implements OnInit{

  descriptionsVisible: boolean;

  constructor(
    public dialogRef: MatDialogRef<ProductTableOptionsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {
    if (this.data.optionsMap.get('descriptions visible') === undefined) {
      this.data.optionsMap.set('descriptions visible', false);
    }
    this.descriptionsVisible = this.data.optionsMap.get('descriptions visible')
  }

  setDescriptionsVisibility(show: boolean) {
    this.data.descriptionMap.forEach((value, key) => this.data.descriptionMap.set(key, show));
    this.data.optionsMap.set('descriptions visible', show);
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductTableOptionsDialogComponent } from './product-table-options-dialog.component';

describe('ProductTableOptionsDialogComponent', () => {
  let component: ProductTableOptionsDialogComponent;
  let fixture: ComponentFixture<ProductTableOptionsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductTableOptionsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductTableOptionsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ProductService} from '../../../services/product/product.service';
import {Product} from '../../../interfaces/product';
import {ProductGroupService} from '../../../services/product-group/product-group.service';
import {ProductGroup} from '../../../interfaces/product-group';
import {Page} from '../../../interfaces/page';
import {Location} from '@angular/common';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {
  addProductForm: FormGroup;
  submitted = false;
  groups: ProductGroup[];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private productService: ProductService,
    private productGroupService: ProductGroupService,
    private location: Location,
  ) {
  }

  ngOnInit(): void {
    this.addProductForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      description: ['', [Validators.required, Validators.maxLength(100)]],
      price: ['', [Validators.required, Validators.min(0)]],
      group: ['', []],
    });

    this.productGroupService.getAllProductGroups(new Page<ProductGroup>()).subscribe(
      response => {
        this.groups = response.content;
      }
    )
  }

  submitForm() {
    this.submitted = true;
    if (this.addProductForm.invalid) {
      return;
    }

    const newProduct = this.convertFormToProduct(this.addProductForm);

    this.productService.addProduct(newProduct).subscribe(
      () => {
        this.router.navigate(['/products']);
      }
    );
  }

  convertFormToProduct(productForm: FormGroup): Product {
    const form: FormGroup = productForm;
    return {
      id: null,
      sold: null,
      name: productForm.value.name,
      description: productForm.value.description,
      price: productForm.value.price,
      groupId: productForm.value.group.id,
    };
  }

  goBack() {
    this.location.back();
  }
}

import {Component, Inject} from '@angular/core';
import {Product} from '../../../interfaces/product';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent {

  constructor(
    public dialogRef: MatDialogRef<ProductDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public product: Product,
    private router: Router
  ) {
  }

  goToUpdateComponent() {
    this.router.navigate(['/products/update/', this.product.id]);
    this.closeDialog();
  }

  closeDialog() {
    this.dialogRef.close();
  }
}

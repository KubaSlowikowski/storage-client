import {Component, OnInit, ViewChild} from '@angular/core';
import {ProductService} from '../../../services/product/product.service';
import {Product} from '../../../interfaces/product';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {Sort, SortDirection} from '@angular/material/sort';
import {Page} from '../../../interfaces/page';
import {ProductGroupService} from '../../../services/product-group/product-group.service';
import {MatDialog} from '@angular/material/dialog';
import {ProductGroupDetailsComponent} from '../../product-groups/product-group-details/product-group-details.component';
import {ProductExportComponent} from '../product-export/product-export/product-export.component';
import {ProductTableOptionsDialogComponent} from '../product-table-options-dialog/product-table-options-dialog.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  columnsToDisplay: string[] = ['id', 'name', 'description', 'price', 'groupId', 'sold', 'options'];
  dataSource: Product[] = [];
  page: Page<Product> = new Page();
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;
  sort: Sort = new class implements Sort {
    active: string = 'id';
    direction: SortDirection = 'asc';
  };
  searchParam: string;
  descriptionMap: Map<number, boolean> = new Map<number, boolean>();
  optionsMap: Map<string, boolean> = new Map<string, boolean>();

  constructor(
    private productService: ProductService,
    private productGroupService: ProductGroupService,
    private downloadDialog: MatDialog,
    private optionsDialog: MatDialog
  ) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit(): void {
    this.page.size = 10;
    this.page.number = 0;
    this.refreshData();
  }

  deleteProduct(product: Product) {
    this.productService.deleteProduct(product.id).subscribe(
      () => {
        this.refreshData();
      }
    );
  }

  buyProduct(product: Product) {
    this.productService.buyProduct(product.id).subscribe(
      () => {
        this.refreshData();
      }
    );
  }

  refreshData(event?: PageEvent) {
    if (event) {
      this.page.number = event.pageIndex;
      this.page.size = event.pageSize;
    }

    if (this.sort.direction === '') {
      this.page.isSorted = false;
    } else {
      this.page.isSorted = true;
    }
    this.page.sortedBy = this.sort.active;
    this.page.dir = this.sort.direction;

    this.productService.getAllProducts(this.page, this.searchParam).subscribe(response => {
        this.page.fromResponse(response);
        this.dataSource = response.content;
        this.pageIndex = response.number;
        this.pageSize = response.size;
        this.length = response.totalElements;

        this.dataSource.forEach(value => {
          this.descriptionMap.set(value.id, false)
        });
      }
    );
    return event;
  }

  openProductGroupDetails(groupId: number): void {
    this.productGroupService.getProductGroupById(groupId).subscribe((group) => {
      this.downloadDialog.open(ProductGroupDetailsComponent, {
        data: {
          id: group.id,
          name: group.name,
          description: group.description,
          products: group.products
        }
      });
    });
  }

  sortData(sort: Sort): void {
    this.sort = sort;
    this.refreshData();
  }

  openDownloadDialog(): void {
    this.refreshData();
    this.downloadDialog.open(ProductExportComponent, {
      data: {
        pageable: this.page.buildPageableUrl(),
        searchParam: this.searchParam
      }
    });
  }

  applyFilter(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      const filterValue = (event.target as HTMLInputElement).value;
      this.refreshData();
      this.searchParam = '&search='.concat(filterValue);
      this.productService.getAllProducts(this.page, this.searchParam).subscribe(response => {
          this.dataSource = response.content;
        }
      );
    }
  }

  showDescription(id: any) {
    this.descriptionMap.set(id, true);
  }

  hideDescription(id: any) {
    this.descriptionMap.set(id, false);
  }

  openTableOptionsDialog() {
    this.optionsDialog.open(ProductTableOptionsDialogComponent, {
      data: {
        descriptionMap: this.descriptionMap,
        optionsMap: this.optionsMap
      }
    })
  }
}

import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProductService} from '../../../services/product/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Product} from '../../../interfaces/product';
import {ProductGroup} from '../../../interfaces/product-group';
import {ProductGroupService} from '../../../services/product-group/product-group.service';
import {Page} from '../../../interfaces/page';
import {Location} from '@angular/common';

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {

  editProductForm: FormGroup;
  id: number;
  sold: boolean;
  isFormSubmitted = false;
  group: ProductGroup;
  groups: ProductGroup[];

  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private productGroupService: ProductGroupService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location,

  ) {
  }

  ngOnInit(): void { //uzupelnianie formularza poprzednimi danymi (przed zmiana)
    this.editProductForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      description: ['', [Validators.required, Validators.maxLength(100)]],
      price: ['', [Validators.required, Validators.min(0)]],
      group: ['', []],
    });

    this.activatedRoute.paramMap.subscribe(params => {
      this.id = +params.get('id');
      this.productService.getProductById(this.id).subscribe(
        receivedProduct => {
          this.productGroupService.getProductGroupById(receivedProduct.groupId).subscribe(
            response => {
              this.editProductForm.setValue({
                name: receivedProduct.name,
                description: receivedProduct.description,
                price: receivedProduct.price,
                group: response,
              });
            }
          );
        }
      )
    });

    this.productGroupService.getAllProductGroups(new Page<ProductGroup>()).subscribe(
      response => {
        this.groups = response.content;
      }
    )
  }

  submitForm() {
    this.isFormSubmitted = true;
    if (this.editProductForm.invalid) {
      return;
    }

    const changedProduct = this.convertFormToProduct(this.editProductForm);

    this.productService.updateProduct(changedProduct).subscribe(
      () => {
        this.router.navigate(['/products']);
      },
    )
  }

  convertFormToProduct(productForm: FormGroup): Product {
    return {
      id: this.id,
      name: productForm.value.name,
      description: productForm.value.description,
      price: productForm.value.price,
      groupId: productForm.value.group.id,
      sold: productForm.value.sold
    };
  }

  goBack() {
    this.location.back();
  }

  compareGroups(g1: ProductGroup, g2: ProductGroup): boolean {
    return g1 && g2 ? g1.id === g2.id : g1 === g2;
  }
}

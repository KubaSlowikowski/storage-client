import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductExportEmailComponent} from './product-export-email.component';

describe('ProductExportEmailComponent', () => {
  let component: ProductExportEmailComponent;
  let fixture: ComponentFixture<ProductExportEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductExportEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductExportEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

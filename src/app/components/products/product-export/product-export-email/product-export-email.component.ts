import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';
import {ProductService} from '../../../../services/product/product.service';
import {Message} from '../../../../interfaces/message';

@Component({
  selector: 'app-product-export-email',
  templateUrl: './product-export-email.component.html',
  styleUrls: ['./product-export-email.component.css']
})
export class ProductExportEmailComponent {
  email = new FormControl('', [Validators.required, Validators.email]);

  constructor(
    public emailDialog: MatDialogRef<ProductExportEmailComponent>,
    @Inject(MAT_DIALOG_DATA) public url: any,
    private productService: ProductService
  ) {
  }

  closeDialog() {
    this.emailDialog.close();
  }

  onSubmit() {
    if (!this.email.valid) {
      return;
    }
    let message = new Message();
    message.sendTo = this.email.value;
    this.productService.sendAllInMail(this.url.pageable, this.url.searchParam, message).subscribe();
    this.emailDialog.close();
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
}

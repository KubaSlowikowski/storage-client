import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ProductService} from '../../../../services/product/product.service';
import {ProductExportEmailComponent} from '../product-export-email/product-export-email.component';

@Component({
  selector: 'app-product-export',
  templateUrl: './product-export.component.html',
  styleUrls: ['./product-export.component.css']
})
export class ProductExportComponent {

  constructor(
    public downloadDialog: MatDialogRef<ProductExportComponent>,
    @Inject(MAT_DIALOG_DATA) public url: any,
    private productService: ProductService,
    private emailDialog: MatDialog
  ) {
  }

  public closeDialog() {
    this.downloadDialog.close();
  }

  downloadAsPdf() {
    this.productService.getAsPdfReport(this.url.pageable, this.url.searchParam).subscribe(x => {
      const blob = new Blob([x], {type: 'application/pdf'});

      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob);
        return;
      }
      const data = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = data;
      link.download = 'testName.pdf';
      link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

      setTimeout(function() {
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    });

    this.downloadDialog.close();
  }

  downloadAsExcel() {
    this.productService.getAsXlsxReport(this.url.pageable, this.url.searchParam).subscribe(x => {
      const blob = new Blob([x], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});

      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob);
        return;
      }
      const data = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = data;
      link.download = 'testName.xlsx';
      link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

      setTimeout(function() {
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    });
  }

  sendInMail() {
    this.emailDialog.open(ProductExportEmailComponent, {
      data: {
        pageable: this.url.pageable,
        searchParam: this.url.searchParam
      }
    });
    this.downloadDialog.close();
  }
}

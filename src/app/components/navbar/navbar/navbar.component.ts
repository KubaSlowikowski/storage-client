import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';
import {MatDialog} from '@angular/material/dialog';
import {AboutContentComponent} from '../about-content/about-content.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLoggedIn = false;
  username: string;

  constructor(
    private tokenStorageService: TokenStorageService,
    private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.username = user.username;
    }
  }

  openDialog() {
    this.dialog.open(AboutContentComponent);
  }
}

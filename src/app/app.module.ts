import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from "@angular/common/http";
import {MatSliderModule} from '@angular/material/slider';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {RouterModule} from '@angular/router';
import {NavbarComponent} from './components/navbar/navbar/navbar.component';
import {ProductComponent} from './components/products/product/product.component';
import {HomeComponent} from './components/home/home.component';
import {ProductGroupComponent} from './components/product-groups/product-group/product-group.component';
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from "@angular/material/button";
import {MatPaginatorModule} from "@angular/material/paginator";
import {ProductUpdateComponent} from './components/products/product-update/product-update.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {ProductCreateComponent} from './components/products/product-create/product-create.component';
import {MatSortModule} from "@angular/material/sort";
import {ProductGroupUpdateComponent} from './components/product-groups/product-group-update/product-group-update.component';
import {ProductGroupCreateComponent} from './components/product-groups/product-group-create/product-group-create.component';
import {MatExpansionModule} from "@angular/material/expansion";
import {MatSelectModule} from "@angular/material/select";
import {LoginComponent} from './components/security/login/login.component';
import {RegisterComponent} from './components/security/register/register.component';
import {ProfileComponent} from './components/security/profile/profile.component';
import {authInterceptorProviders} from './_helpers/auth/auth.interceptor';
import {MatCardModule} from "@angular/material/card";
import {MatMenuModule} from "@angular/material/menu";
import { AboutContentComponent } from './components/navbar/about-content/about-content.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatRippleModule} from "@angular/material/core";
import { ProductDetailsComponent } from './components/products/product-details/product-details.component';
import { ProductGroupDetailsComponent } from './components/product-groups/product-group-details/product-group-details.component';
import { ProductExportComponent } from './components/products/product-export/product-export/product-export.component';
import { ProductGroupExportComponent } from './components/product-groups/product-group-export/product-group-export/product-group-export.component';
import { globalHttpInterceptorProviders } from './_helpers/global-http/global-http-interceptor';
import { ErrorDialogComponent } from './components/error-dialog/error-dialog.component';
import { ProductExportEmailComponent } from './components/products/product-export/product-export-email/product-export-email.component';
import { ProductGroupExportEmailComponent } from './components/product-groups/product-group-export/product-group-export-email/product-group-export-email.component';
import {FooterComponent} from './components/footer/footer.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { ProductTableOptionsDialogComponent } from './components/products/product-table-options-dialog/product-table-options-dialog.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { ProductGroupTableOptionsDialogComponent } from './components/product-groups/product-group-table-options-dialog/product-group-table-options-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ProductComponent,
    HomeComponent,
    ProductGroupComponent,
    ProductUpdateComponent,
    ProductCreateComponent,
    ProductGroupUpdateComponent,
    ProductGroupCreateComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    AboutContentComponent,
    ProductDetailsComponent,
    ProductGroupDetailsComponent,
    ProductExportComponent,
    ProductGroupExportComponent,
    ErrorDialogComponent,
    ProductExportEmailComponent,
    ProductGroupExportEmailComponent,
    FooterComponent,
    ProductTableOptionsDialogComponent,
    ProductGroupTableOptionsDialogComponent
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatSliderModule,
        MatToolbarModule,
        MatIconModule,
        RouterModule.forRoot([
            {path: 'home', component: HomeComponent},
            {path: 'products', component: ProductComponent},
            {path: 'products/update/:id', component: ProductUpdateComponent},
            {path: 'products/add', component: ProductCreateComponent},
            {path: 'groups', component: ProductGroupComponent},
            {path: 'groups/add', component: ProductGroupCreateComponent},
            {path: 'groups/update/:id', component: ProductGroupUpdateComponent},

            {path: 'login', component: LoginComponent},
            {path: 'register', component: RegisterComponent},
            {path: 'profile', component: ProfileComponent},
            {path: '', redirectTo: 'home', pathMatch: 'full'}
        ]),
        MatTableModule,
        MatButtonModule,
        MatPaginatorModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatSortModule,
        MatExpansionModule,
        MatSelectModule,
        FormsModule,
        MatCardModule,
        MatMenuModule,
        MatDialogModule,
        MatRippleModule,
        MatGridListModule,
        MatCheckboxModule
    ],
  providers: [
    authInterceptorProviders,
    globalHttpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule {
}
